"""
A simple wrapper for calling the KETJU integrator from Python. 
"""
from .bindings import *
from .merger_remnant_properties import normalized_merger_properties
